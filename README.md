# Demo

https://jade-exam-demo.netlify.com/

# Coding Challenge

Built with [React](https://facebook.github.io/react/), [Redux Toolkit](https://redux-toolkit.js.org/), [React Reveal](https://www.react-reveal.com/), [React Router](https://github.com/ReactTraining/react-router), [React Spinners](https://www.davidhu.io/react-spinners/), [Styled Components](https://styled-components.com/), [React-Grid-System](https://github.com/sealninja/react-grid-system)

## Quick Start

#### 1. Type `yarn install` or `npm install`

Install all dependencies.

#### 2. Type `yarn start` or `npm start`

Runs the app in the development mode.
Open http://localhost:3000

#### 3. Type `yarn test` or `npm test`

Run a unit test
