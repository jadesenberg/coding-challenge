const darkModeReducer = (state = {}, action) => {
	switch (action.type) {
		case "TOGGLE_DARK_MODE":
			return {
				isDark: !state.isDark,
				mode: state.isDark ? "dark" : "light",
			};
		default:
			return state;
	}
};

export default darkModeReducer;
