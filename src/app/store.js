import { configureStore } from "@reduxjs/toolkit";
import cardReducer from "../features/card/cardSlice";

export default configureStore({
	reducer: {
		cards: cardReducer,
	},
});
