import React, { lazy, Suspense, useReducer } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import styled, { ThemeProvider, createGlobalStyle } from "styled-components";

import darkModeReducer from "reducers/darkModeReducer";
import DarkModeContext from "contexts/darkModeContext";
import { light, dark } from "utils/themes";

import Header from "components/Header";
import Footer from "components/Footer";
import Spinner from "components/Common/Spinner";
import DarkModeSwitch from "components/DarkModeSwitch";

const Home = lazy(() => import("pages/HomePage"));
const View = lazy(() => import("pages/ViewPage"));

const NoMatchPage = () => {
	return <h3>404 - Not found </h3>;
};

const routes = [
	{ id: 1, path: "/view/:id", component: View },
	{ id: 2, path: "/", exact: true, component: Home },
	{ id: 3, component: NoMatchPage },
];

const MainContainer = styled.div`
	margin-left: 5%;
	margin-right: 5%;
	margin-top: 1%;
`;

const Space = styled.div`
	height: 60px;
`;

const FootSpace = styled.div`
	height: 50px;
`;

export const GlobalStyles = createGlobalStyle`
  body, #root {
    background: ${({ theme }) => theme.background};
    color: ${({ theme }) => theme.text};
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    font-family: BlinkMacSystemFont, -apple-system, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;
  }
`;

function App() {
	const [state, dispatch] = useReducer(darkModeReducer, {
		isDark: false,
		mode: "dark",
	});

	return (
		<DarkModeContext.Provider value={{ state, dispatch }}>
			<ThemeProvider theme={state.isDark ? dark : light}>
				<GlobalStyles />

				<MainContainer>
					<Router>
						<Header />
						<DarkModeSwitch />
						<Space />
						<Suspense fallback={<Spinner />}>
							<Switch>
								{routes.map((route) => (
									<Route
										key={route.id}
										exact={route.exact}
										path={route.path}
										component={route.component}
									/>
								))}
							</Switch>
						</Suspense>

						<FootSpace />
						<hr />
						<Footer />
					</Router>
				</MainContainer>
			</ThemeProvider>
		</DarkModeContext.Provider>
	);
}

export default App;
