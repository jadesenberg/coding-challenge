const black = "#111111";
const white = "white";

export const light = {
	text: black,
	background: white,
};

export const dark = {
	text: white,
	background: black,
};
