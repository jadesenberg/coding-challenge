import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const MainContainer = styled.div`
	display: flex;
	margin-bottom: 30px;
`;

const LogoLink = styled(Link)`
	box-shadow: none;
	text-decoration: none;
	color: inherit;
`;

const FooterNav = styled.div`
	float: right;
	width: 100%;
	margin-top: auto;
	margin-bottom: auto;
`;

const NavListContainer = styled.ul`
	list-style-type: none;
	float: right;
	height: 25px;
`;

const ListContainer = styled.li`
	float: left;
	margin-left: 40px;
	font-family: Arial;
	font-size: 13px;
`;

const ListLinkContainer = styled(Link)`
	box-shadow: none;
	text-decoration: none;
	color: inherit;
`;

const LogoLinkText = styled.h1`
	font-size: 24px;
	font-family: Arial;
`;

function Footer() {
	return (
		<MainContainer>
			<LogoLink to={`/`}>
				<LogoLinkText>ADRENALIN</LogoLinkText>
			</LogoLink>

			<FooterNav>
				<NavListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>Privacy</ListLinkContainer>
					</ListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>Sitemap</ListLinkContainer>
					</ListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>Facebook</ListLinkContainer>
					</ListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>Linkedin</ListLinkContainer>
					</ListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>
							Instagram
						</ListLinkContainer>
					</ListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>Twitter</ListLinkContainer>
					</ListContainer>
				</NavListContainer>
			</FooterNav>
		</MainContainer>
	);
}

export default Footer;
