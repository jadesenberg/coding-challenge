import React from "react";
import { render } from "@testing-library/react";
import Footer from ".";
import { BrowserRouter } from "react-router-dom";

test("Render Footer", () => {
	const { getByText } = render(
		<BrowserRouter>
			<Footer />
		</BrowserRouter>
	);
	expect(getByText("Sitemap"));
});
