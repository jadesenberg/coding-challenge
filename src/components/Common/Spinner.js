import React from "react";
import BeatLoader from "react-spinners/BeatLoader";
import styled from "styled-components";

const Container = styled.div`
	display: flex;
	justify-content: center;
	margin-top: 100px;
	margin-bottom: 100px;
`;

const Spinner = () => {
	return (
		<Container>
			<BeatLoader size={20} color={"black"} loading={true} />
		</Container>
	);
};

export default Spinner;
