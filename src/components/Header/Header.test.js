import React from "react";
import { render } from "@testing-library/react";
import Header from ".";
import { BrowserRouter } from "react-router-dom";

test("Render Header", () => {
	const { getByText } = render(
		<BrowserRouter>
			<Header />
		</BrowserRouter>
	);
	expect(getByText("Culture"));
});
