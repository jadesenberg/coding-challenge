import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const MainContainer = styled.div`
	display: flex;
	margin-bottom: 30px;
`;

const LogoLink = styled(Link)`
	box-shadow: none;
	text-decoration: none;
	color: inherit;
`;

const HeaderNav = styled.div`
	float: right;
	width: 100%;
	margin-top: auto;
	margin-bottom: auto;
`;

const NavListContainer = styled.ul`
	list-style-type: none;
	float: right;
	height: 25px;
`;

const ListContainer = styled.li`
	float: left;
	margin-left: 40px;
	font-family: Arial;
	font-size: 13px;
`;

const ListLinkContainer = styled(Link)`
	box-shadow: none;
	text-decoration: none;
	color: inherit;
`;

const LogoLinkText = styled.h1`
	font-size: 24px;
	font-family: Arial;
`;

function Header() {
	return (
		<MainContainer>
			<LogoLink to={`/`}>
				<LogoLinkText>ADRENALIN</LogoLinkText>
			</LogoLink>

			<HeaderNav>
				<NavListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>Culture</ListLinkContainer>
					</ListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>Work</ListLinkContainer>
					</ListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>Clients</ListLinkContainer>
					</ListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>Services</ListLinkContainer>
					</ListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>Carrers</ListLinkContainer>
					</ListContainer>
					<ListContainer>
						<ListLinkContainer to={`/`}>Contact</ListLinkContainer>
					</ListContainer>
				</NavListContainer>
			</HeaderNav>
		</MainContainer>
	);
}

export default Header;
