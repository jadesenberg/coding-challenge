import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const StyledContainer = styled.div`
	background: white;
	display: flex;
	flex-direction: column;
`;

const Title = styled.h4`
	font-weight: bold;
	font-family: Arial;
	font-size: 24px;
	height: 30px;
	color: black;
	margin-left: 10px;
`;

const StyledPhoto = styled.img`
	width: 100%;
	height: 100%;
	object-fit: cover;
	border: black;
`;

const View = styled(Link)`
	color: blue;
	text-decoration: none;
	font-size: 13px;
	margin-left: 10px;
`;

const ViewText = styled.h1`
	overflow: hidden;

	letter-spacing: 1px;
	font-family: Arial;
	font-size: 15px;
	font-weight: bold;

	&::before {
		background-color: blue;
		content: "";
		display: inline-block;
		height: 1px;
		position: relative;
		vertical-align: middle;
		width: 6%;
	}

	&::before {
		right: 1em;
	}
`;

const Tag = styled.div`
	position: absolute;
	top: 37px;
	left: -23px;
	width: 100px;
	height: 25px;
	background-color: white;
	color: gray;
	transform: rotate(-90deg);
	font-family: Georgia;
	font-size: 11px;
	text-align: right;
	text-transform: uppercase;
`;

const Card = ({ title, img, url, tag }) => (
	<StyledContainer>
		<StyledPhoto src={img} data-testid="photo" alt={title} />
		<Tag>{tag}</Tag>
		<Title data-testid="title">{title}</Title>
		<View to={"/view/" + url}>
			<ViewText data-testid="url">VIEW CASE STUDY</ViewText>
		</View>
	</StyledContainer>
);
export default Card;
