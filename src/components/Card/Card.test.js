import React from "react";
import { render } from "@testing-library/react";
import Card from ".";
import { BrowserRouter } from "react-router-dom";

test("Render Card", () => {
	const { getByTestId, getByAltText } = render(
		<BrowserRouter>
			<Card
				title="title"
				img="https://bitbucket.org/adrenalingit/coding-challenge-2019/raw/2bcc675cb6d5a7a6d8065f64d697b459947b883e/assets/1001.jpg"
				url="VIEW CASE STUDY"
			/>
		</BrowserRouter>
	);
	expect(getByTestId("title").textContent).toBe("title");
	expect(getByTestId("url").textContent).toBe("VIEW CASE STUDY");

	expect(getByAltText("title"));
});
