import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
	loading: false,
	hasErrors: false,
	cards: [],
};

export const cardSlice = createSlice({
	name: "cards",
	initialState,
	reducers: {
		getCards: (state) => {
			state.loading = true;
		},
		getCardsSuccess: (state, { payload }) => {
			state.cards = payload;
			state.loading = false;
			state.hasErrors = false;
		},
		getCardsFailure: (state) => {
			state.loading = false;
			state.hasErrors = true;
		},
		getCardById: (state, action) => {
			const data = state.cards.filter((card) => {
				return card.id == action.payload;
			});
			state.cards = data;
		},
	},
});

// Three actions generated from the slice
export const {
	getCards,
	getCardsSuccess,
	getCardsFailure,
	getCardById,
} = cardSlice.actions;

// A selector
export const cardsSelector = (state) => state.cards;

export default cardSlice.reducer;

// Asynchronous thunk action
export function fetchCards() {
	return async (dispatch) => {
		dispatch(getCards());

		try {
			const response = await fetch(
				"https://bitbucket.org/adrenalingit/coding-challenge-2019/raw/2bcc675cb6d5a7a6d8065f64d697b459947b883e/feed/data.json"
			);
			const data = await response.json();

			dispatch(getCardsSuccess(data));
		} catch (error) {
			dispatch(getCardsFailure());
		}
	};
}

export function fetchCard(id) {
	return async (dispatch) => {
		dispatch(getCardById(id));
	};
}
