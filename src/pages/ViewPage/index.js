import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { cardsSelector, fetchCard } from "features/card/cardSlice";
import { useParams, Link } from "react-router-dom";

import styled from "styled-components";
import { Container, Row, Col } from "react-grid-system";

const StyledPhoto = styled.img`
	height: 100%;
	object-fit: cover;
	border: black;
	width: 100%;
`;

const TitleContainer = styled.h1`
	font-size: 64px;
	font-family: Arial;
	font-weight: bold;
	margin-left: 5%;
`;
const QuestionsContainer = styled.ul`
	list-style-type: none;
`;
const QuestionTitle = styled.h3`
	font-size: 32px;
	font-family: Arial;
	font-weight: bold;
`;
const QuestionBody = styled.h4`
	font-size: 21px;
	font-family: Georgia;

	font-weight: normal;
`;
const InfoContainer = styled.div`
	width: 80%;
	float: right;
`;

const Tag = styled.div`
	position: absolute;
	top: 37px;
	left: -23px;
	width: 100px;
	height: 25px;
	background-color: white;
	color: gray;
	transform: rotate(-90deg);
	font-family: Georgia;
	font-size: 11px;
	text-align: right;
	text-transform: uppercase;
`;

function ViewPage() {
	const dispatch = useDispatch();

	const { cards } = useSelector(cardsSelector);
	const { id } = useParams();
	let counter = 0;

	useEffect(() => {
		dispatch(fetchCard(id));
	}, [id, dispatch]);

	return (
		<Container style={{ margin: "0", padding: "0", maxWidth: "none" }}>
			{cards.length > 0 ? (
				<Row>
					<Col sm={6}>
						<StyledPhoto
							src={require("assets/" + cards[0].image)}
						/>
						<Tag>{cards[0].tag}</Tag>
					</Col>
					<Col sm={6}>
						<InfoContainer>
							<TitleContainer>{cards[0].title}</TitleContainer>
							<QuestionsContainer>
								{cards[0].questions.map((question) => {
									counter++;
									return (
										<li key={counter}>
											<QuestionTitle>
												Question {counter}{" "}
											</QuestionTitle>
											<QuestionBody>
												{question}
											</QuestionBody>
										</li>
									);
								})}
							</QuestionsContainer>
						</InfoContainer>
					</Col>
				</Row>
			) : (
				<div>
					Oooopss something went wrong! <Link to="/">Reload</Link>
				</div>
			)}
		</Container>
	);
}

export default ViewPage;
