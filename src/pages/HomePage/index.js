import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Container, Row, Col } from "react-grid-system";
import styled from "styled-components";
import Fade from "react-reveal/Fade";

import { cardsSelector, fetchCards } from "features/card/cardSlice";

import Card from "components/Card";
import Spinner from "components/Common/Spinner";

const StyledContainer = styled.div`
	width: 100%;
	margin: 0;
	padding: 0;
`;

const Spacing = styled.div`
	height: 50px;
`;

function HomePage() {
	const dispatch = useDispatch();
	const { cards, loading, hasErrors } = useSelector(cardsSelector);

	useEffect(() => {
		dispatch(fetchCards());
	}, [dispatch]);

	return (
		<Container style={{ margin: "0", padding: "0", maxWidth: "none" }}>
			{hasErrors && <div>Something went wrong ...</div>}
			{loading ? (
				<Spinner />
			) : (
				<Fade>
					<Row>
						{cards.map((card) => (
							<Col sm={6} key={card.id}>
								<StyledContainer>
									<Card
										img={require("assets/" + card.thumb)}
										title={card.title_long}
										tag={card.tag}
										url={card.id}
									/>
								</StyledContainer>
								<Spacing />
							</Col>
						))}
					</Row>
				</Fade>
			)}
		</Container>
	);
}

export default HomePage;
